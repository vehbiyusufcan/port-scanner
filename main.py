from os import startfile
import pyfiglet
import socket
import sys
from datetime import  datetime

def banner():
    ascii_banner = pyfiglet.figlet_format("PORT SCANNER")
    print(ascii_banner)

def portscanner(remotehost,portrange):
    print("-" * 60)
    print("Please wait, scanning remote host", remoteServerIp)
    print("-" * 60)
    try:
        for port in range(1,portrange):
            sock = socket.socket(socket.AF_INET ,socket.SOCK_STREAM)
            result = sock.connect_ex((remoteServerIp,port))
            if result == 0:
                print("Port {} is open".format(port))
            sock.close()
    except KeyboardInterrupt:
        print("You pressed Ctrl+C")
        sys.exit()

    except socket.gaierror:
        print("Hostname could not be resolved. Exiting")
        sys.exit()

    except socket.error:
        print("Couldn't connect to server")
        sys.exit()

def time():
    return datetime.now()

def timediff(stime,ftime):
    return (ftime - stime)

if __name__ == "__main__":
    banner()
    remoteServer = input("Enter a remote host to scane: ")
    portrange = input("Enter a port range to scane: ")
    remoteServerIp = socket.gethostbyname(remoteServer)
    started_time = time()
    portscanner(remoteServerIp,int(portrange))
    finiseh_time = time()
    total_time = timediff(started_time,finiseh_time)
    print(f"scanning completed in {total_time}")